@extends('layouts.default')
@section('title', 'Cari solusimu disini')

@section('content')
<style>
    .cursor-hover:hover {
        cursor: pointer;
    }
</style>

<div class='container-fluid'>
    @if($questions->count())
        <div class='row'>
            <div class='col py-2'>
                <div class="card border-left-warning shadow h-100 py-2">
                    <div class="card-body">
                      <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                Total Pertanyaan
                            </div>
                          <div class="h5 mb-0 font-weight-bold text-gray-800">
                              {{ $questions->count() }}
                          </div>
                        </div>
                        <div class="col-auto">
                          <i class="fas fa-comments fa-2x text-gray-300"></i>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
        
        @foreach ($questions as $question)
        <div class='row'>
            <div class='col'>
                <div 
                    class="card shadow mb-4 cursor-hover" 
                    onclick="location.assign('{{ route('post.show', ['post' => $question->id]) }}')"
                >
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">
                            <a href="{{ route('post.show', ['post' => $question->id]) }}">
                            {{ $question->title }}
                        </a>
                        </h6>
                    </div>
                    <div class="card-body">
                        {!! $question->post->content !!}
                    </div>
                    <div class="card-footer">
                        By {{ $question->post->user->name }},
                        {{ $question->post->ago }}
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    @else
        <h1>Buat pertanyaan pertama mu disini!</h1>
        <a href="{{ route('post.create') }}" class='btn btn-primary'>
            Buat Satu
        </a>
    @endif
</div>
@endsection
