<?php
    $content = $post->title;
    $desc    = $post->content;
    $vote    = 0;
    $by      = $post->user;
    $time    = $post->time;
?>

@extends('layouts.default')
@section('title', "Diskusi {$content}")

@section('content')
<div class='container-fluid pb-5'>
    <div class='row'>
        <div class='col-1'>
            <div class='d-flex flex-column'>
                <div class="text-center">
                    <a href="#" style="font-size:40px;" >
                        <i class="fas fas-xl fa-caret-up"></i>
                    </a>
                </div>
                <div class="font-weight-bold h4 text-center">
                    {{ $vote }}
                </div>
                <div class="text-center">
                    <a href="#" style="font-size:40px;" >
                        <i class="fas fa-caret-down"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-11">
            <h1>{{ $content }}</h1>
            <div>
                {!! $desc !!}
            </div>
            <div>
                <sub>
                    {{ $by }} at {{ $time }} <br />
                    @if(Auth::user())
                        @if(Auth::user()->id == $post->user_id)
                            <a href="{{ route('post.edit', ['post' => $post->id ]) }}">Ubah</a>
                        @endif
                    @endif
                </sub>
            </div>
        </div>
    </div>
</div>

<div class='container-fluid'>
    <div class='row'>
        <div class='col'>
            <h3>Jawaban</h3>
            <hr class="sidebar-divider">
        </div>
    </div>
    <div class='row'>
        <div class='col-1'>
            <div class='d-flex flex-column'>
                <div class="text-center">
                    <a href="#" style="font-size:40px;" >
                        <i class="fas fas-xl fa-caret-up"></i>
                    </a>
                </div>
                <div class="font-weight-bold h4 text-center">
                    {{ $vote }}
                </div>
                <div class="text-center">
                    <a href="#" style="font-size:40px;" >
                        <i class="fas fa-caret-down"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-11">
            <div>
                {!! $desc !!}
            </div>
            <div>
                <sub>
                    {{ $by }} at {{ $time }}
                </sub>
            </div>
        </div>
    </div>
</div>
@endsection