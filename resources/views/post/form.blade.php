@extends('layouts.default')
@section('title', 'Buat Satu Punya Mu')

<?php
    $is_update = isset($post);
    if($is_update) {
        $config['form_title']    = "Ubah Pertanyaanmu";
        $config['url']          = route('post.update', ['post' => $post->id]);
        $config['method']       = "PUT";
    }else{
        $config['form_title']   = "Buat Pertanyaan Baru";
        $config['url']          = route('post.store');
        $config['method']       = "POST";
    }
?>

@section('content')
<div class='container-fluid'>
    <h1>{{ $config['form_title'] }}</h1>
    <div id='form'>
        <form method="POST" action="{{ $config['url'] }}">
            @csrf
            @method($config['method'])
            <div class="form-group">
                <div class="col">
                    <input 
                        id="title" 
                        type="text" 
                        class="form-control @error('title') is-invalid @enderror" 
                        name="title" 
                        value="{{ old('title', isset($title) ? $title : '') }}" 
                        required 
                        placeholder="Judul"
                        autocomplete="title" 
                        autofocus
                    />
    
                    @error('title')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
    
            <div class="form-group">
                <div class="col">
                    <textarea 
                        id="desc" 
                        class="form-control @error('desc') is-invalid @enderror" 
                        name="desc" 
                        hidden
                        required 
                        placeholder="Please write detail your problem here.."
                        autocomplete="title" 
                        autofocus
                    >
                        {{ old('desc', $is_update ? $post->content : '') }}
                    </textarea>
    
                    @error('desc')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
    
            <div class="form-group">
                <div class='col'>
                    <button class="btn btn-primary">
                        Tanya Ini
                    </button>

                    <button id='preview_btn' class="btn btn-primary">
                        Coba Liat
                    </button>
                </div>
            </div>
        </form>
    </div>

    <div id='preview'>
        
    </div>
</div>

<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
    tinymce.init({selector:'#desc'});

    var preview_btn = document.getElementById('preview_btn');
    preview_btn.addEventListener('click', function(e) {
        e.preventDefault();

        var title = document.getElementById('title').value;
        var desc  = document.getElementById('desc').value;

        console.log(title, desc);
    });
</script>
@endsection