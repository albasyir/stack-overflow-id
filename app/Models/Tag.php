<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $guarded = [];

    public function questions()
    {
        return $this->belongsToMany('App\Models\Question', 'questions_tags', 'tag_id', 'question_id');
    }
}
