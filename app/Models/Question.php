<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $guarded = [];

    public function post()
    {
        return $this->hasOne('App\Models\Post', 'id', 'post_id');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag', 'questions_tags', 'question_id', 'tag_id');
    }
}
